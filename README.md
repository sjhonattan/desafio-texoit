# Avaliação Técnica - Texo it

Este projeto é referente a avaliação técnica como requisito a oportunidade de desenvolvedor JAVA.

## Execução

Para executar o projeto bastar rodar o seguinte comando:

```bash
mvn clean install
./mvnw spring-boot:run

```

## Testes

Para executar os testes do projeto bastar rodar o seguinte comando:

```bash
./mvnw test
```
