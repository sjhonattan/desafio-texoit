package com.texoit.goldenrbaward.app.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WorstMovieControllerTests {

    @LocalServerPort
    private int port;
    
    private String basePath = "/api/v1";

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();
    
    @Test
    public void testGetIntervalAwards() throws Exception {
    	
    	
    	String url = "http://localhost:" + port + basePath +"/interval-awards";
    	
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        String expected = "{" + 
        		"    \"min\": [" + 
        		"        {" + 
        		"            \"producer\": \"Joel Silver\"," + 
        		"            \"interval\": 1," + 
        		"            \"previousWin\": 1990," + 
        		"            \"followingWin\": 1991" + 
        		"        }" + 
        		"    ]," + 
        		"    \"max\": [" + 
        		"        {" + 
        		"            \"producer\": \"Matthew Vaughn\"," + 
        		"            \"interval\": 13," + 
        		"            \"previousWin\": 2002," + 
        		"            \"followingWin\": 2015" + 
        		"        }" + 
        		"    ]" + 
        		"}";

        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

}