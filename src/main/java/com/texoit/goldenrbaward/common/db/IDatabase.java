package com.texoit.goldenrbaward.common.db;

import com.texoit.goldenrbaward.common.exception.DatasetLoadException;

public interface IDatabase {
	void connect()throws DatasetLoadException;
}
