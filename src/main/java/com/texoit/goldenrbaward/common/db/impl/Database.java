package com.texoit.goldenrbaward.common.db.impl;

import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.opencsv.bean.CsvToBeanBuilder;
import com.texoit.goldenrbaward.common.db.IDatabase;
import com.texoit.goldenrbaward.common.exception.DatasetLoadException;
import com.texoit.goldenrbaward.data.datasource.local.IMovieDao;
import com.texoit.goldenrbaward.data.datasource.local.IProducerDao;
import com.texoit.goldenrbaward.data.datasource.local.IStudioDao;
import com.texoit.goldenrbaward.data.datasource.local.mapper.MovieCsvMapper;
import com.texoit.goldenrbaward.data.models.MovieModel;
import com.texoit.goldenrbaward.data.models.ProducerModel;
import com.texoit.goldenrbaward.data.models.StudioModel;

@Component
public class Database implements IDatabase{
	private final char SEPARATOR = ';';
	
	@Value("${dataset.classpath}")
	private String datasetClasspath;
	
	@Autowired
	private IMovieDao movieDao;
	
	@Autowired
	private IStudioDao studioDao;
	
	@Autowired
	private IProducerDao producerDao;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void connect() throws DatasetLoadException {
		
		try {
			System.out.println("===== LOADING DATASET := "+datasetClasspath);
			
			if(datasetClasspath == null) {
				throw new DatasetLoadException("Dataset classpath is not configured.");
			}
			
			List<MovieCsvMapper> beans = new CsvToBeanBuilder(new FileReader(ResourceUtils.getFile(datasetClasspath)))
	                .withType(MovieCsvMapper.class)
	                .withSeparator(SEPARATOR)
	                .withSkipLines(1)
	                .build()
	                .parse();
			
			List<MovieModel> movies = beans.stream().map(e->e.parseH2()).collect(Collectors.toList());
			
			for (MovieModel movie : movies) {
					for (ProducerModel p : movie.getProducers()) {
						ProducerModel x = producerDao.findBySlug(p.getSlug());
						
						if(x == null) {
							x = producerDao.save(p);
						}
						p.setId(x.getId());
					}
					
					for (StudioModel s : movie.getStudios()) {
						StudioModel x = studioDao.findBySlug(s.getSlug());
						
						if(x == null) {
							x = studioDao.save(s);
						}
						s.setId(x.getId());
					}
				
					movieDao.save(movie);
			}
			
			System.out.println("===== DATASET LOADED := "+datasetClasspath);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DatasetLoadException(e);
		}
	}
	
}
