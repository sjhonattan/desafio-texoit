package com.texoit.goldenrbaward.common.exception;

@SuppressWarnings("serial")
public class DatasetLoadException extends Exception{

	public DatasetLoadException(Exception e) {
		super(e);
	}

	public DatasetLoadException(String string) {
		super(string);
	}

}
