package com.texoit.goldenrbaward.common.usecase;

public interface IUseCase<R> {
	R call() throws Exception;
}
