package com.texoit.goldenrbaward;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.texoit.goldenrbaward.common.db.impl.Database;
import com.texoit.goldenrbaward.common.exception.DatasetLoadException;
import com.texoit.goldenrbaward.data.repositories.ProducerRepository;
import com.texoit.goldenrbaward.domain.repositories.IProducerRepository;

@Configuration
public class AppConfig {

	@Autowired
	private Database database;
	
    @Bean
    public IProducerRepository repository() {
        return new ProducerRepository();
    }
    
    @EventListener(ApplicationReadyEvent.class)
    public void loadDatasetFromLocalCsvFile() throws DatasetLoadException {
    	database.connect();
    }

}