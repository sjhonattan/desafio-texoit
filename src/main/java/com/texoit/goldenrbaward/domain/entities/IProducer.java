package com.texoit.goldenrbaward.domain.entities;

import java.util.List;

public interface IProducer {
	
	public Long getId();
	
	public String getName();
	
	public String getSlug();
	
}
