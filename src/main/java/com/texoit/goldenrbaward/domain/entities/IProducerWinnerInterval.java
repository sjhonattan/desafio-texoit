package com.texoit.goldenrbaward.domain.entities;

public interface IProducerWinnerInterval {
	public String getProducer();

	public Integer getInterval();

	public Integer getPreviousWin();

	public Integer getFollowingWin();
}
