package com.texoit.goldenrbaward.domain.entities;

public interface IMovie {
	public Long getId();
	
	public Integer getYear();

	public String getTitle();

	public Boolean getWinner();
	
}
