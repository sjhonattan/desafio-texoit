package com.texoit.goldenrbaward.domain.entities;

public interface IStudio {
	public Long getId();

	public String getName();
	
	public String getSlug();
	

}
