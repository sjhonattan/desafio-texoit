package com.texoit.goldenrbaward.domain.repositories;

import java.util.List;

import com.texoit.goldenrbaward.domain.entities.IProducerWinnerInterval;

public interface IProducerRepository {
	List<IProducerWinnerInterval>  getProducersWinningIntervalWith2orMoreAwards(boolean minorInterval) throws Exception;
}
