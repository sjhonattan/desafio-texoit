package com.texoit.goldenrbaward.domain.usecases;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.texoit.goldenrbaward.common.usecase.IUseCase;
import com.texoit.goldenrbaward.domain.entities.IProducerWinnerInterval;
import com.texoit.goldenrbaward.domain.repositories.IProducerRepository;

@Component
public class GetProducerWithLongestIntervalBetween2ConsecutiveAwards implements IUseCase<List<IProducerWinnerInterval>>{

	@Autowired
	private IProducerRepository repository;

	@Override
	public List<IProducerWinnerInterval> call() throws Exception {
		
		List<IProducerWinnerInterval> producersWinningInterval = this.repository.getProducersWinningIntervalWith2orMoreAwards(false);
		
		Integer interval = producersWinningInterval.stream().mapToInt(IProducerWinnerInterval::getInterval).max().getAsInt();
		
		return producersWinningInterval.stream().filter(p -> p.getInterval() == interval).collect(Collectors.toList());
		
	}
	
	
	
}
