package com.texoit.goldenrbaward;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoldenrbawardApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoldenrbawardApplication.class, args);
	}

}
