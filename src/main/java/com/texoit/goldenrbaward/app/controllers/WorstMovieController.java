package com.texoit.goldenrbaward.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.goldenrbaward.app.models.BaseResponse;
import com.texoit.goldenrbaward.app.models.ErrorResponse;
import com.texoit.goldenrbaward.app.models.IntervalAwardsResponse;
import com.texoit.goldenrbaward.domain.entities.IProducerWinnerInterval;
import com.texoit.goldenrbaward.domain.usecases.GetProducerWhoWon2AwardsFaster;
import com.texoit.goldenrbaward.domain.usecases.GetProducerWithLongestIntervalBetween2ConsecutiveAwards;

@RestController
@RequestMapping("/api/v1")
public class WorstMovieController {

	@Autowired
	private GetProducerWithLongestIntervalBetween2ConsecutiveAwards getProducerWithLongestIntervalBetween2ConsecutiveAwards;
	
	@Autowired
	private GetProducerWhoWon2AwardsFaster getProducerWhoWon2AwardsFaster;

	@GetMapping("/interval-awards")
	public @ResponseBody ResponseEntity<? extends BaseResponse> getIntervalAwards() {

		try {
			
			List<IProducerWinnerInterval> min = getProducerWhoWon2AwardsFaster.call();
			List<IProducerWinnerInterval> max = getProducerWithLongestIntervalBetween2ConsecutiveAwards.call();
			
			
			return new ResponseEntity<IntervalAwardsResponse>(new IntervalAwardsResponse(min,max), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
