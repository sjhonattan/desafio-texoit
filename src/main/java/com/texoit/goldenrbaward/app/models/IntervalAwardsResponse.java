package com.texoit.goldenrbaward.app.models;

import java.util.List;

import com.texoit.goldenrbaward.domain.entities.IProducerWinnerInterval;

public class IntervalAwardsResponse extends BaseResponse{
	private List<IProducerWinnerInterval> min;
	private List<IProducerWinnerInterval> max;
	
	public IntervalAwardsResponse(List<IProducerWinnerInterval> min, List<IProducerWinnerInterval> max) {
		super();
		this.min = min;
		this.max = max;
	}

	public List<IProducerWinnerInterval> getMin() {
		return min;
	}

	public void setMin(List<IProducerWinnerInterval> min) {
		this.min = min;
	}

	public List<IProducerWinnerInterval> getMax() {
		return max;
	}

	public void setMax(List<IProducerWinnerInterval> max) {
		this.max = max;
	}

}
