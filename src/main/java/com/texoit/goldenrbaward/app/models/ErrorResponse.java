package com.texoit.goldenrbaward.app.models;

public class ErrorResponse extends BaseResponse {
	protected Integer code;
	protected String error;

	public ErrorResponse(Integer code, String error) {
		super();
		this.code = code;
		this.error = error;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
