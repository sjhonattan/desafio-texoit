package com.texoit.goldenrbaward.data.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;
import com.texoit.goldenrbaward.domain.entities.IMovie;

@Entity(name = "Movie")
@Table(name = "tb_movie")
public class MovieModel implements IMovie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_movie")
	private Long id;

	@NotNull
	@Column(name = "year")
	private Integer year;

	@NotNull
	@Column(name = "title")
	private String title;

	@NotNull
	@Column(name = "winner")
	private Boolean winner;

	@ManyToMany(cascade = { CascadeType.MERGE})
	@JoinTable(name = "tb_movie_producer", joinColumns = @JoinColumn(name = "id_movie"), inverseJoinColumns = @JoinColumn(name = "id_producer"))
	private Set<ProducerModel> producers = new HashSet<>();

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "tb_movie_studio", joinColumns = @JoinColumn(name = "id_movie"), inverseJoinColumns = @JoinColumn(name = "id_studio"))
	private Set<StudioModel> studios = new HashSet<>();

	
	public void addProducer(ProducerModel producer) {
		producers.add(producer);
		producer.getMovies().add(this);
    }
 
    public void removeProducer(ProducerModel producer) {
    	producers.remove(producer);
        producer.getMovies().remove(this);
    }
    
    public void addStudio(StudioModel studio) {
    	studios.add(studio);
    	studio.getMovies().add(this);
    }
 
    public void removeStudio(StudioModel studio) {
    	studios.remove(studio);
    	studio.getMovies().remove(this);
    }
	
	
	@Override
	public Integer getYear() {
		return this.year;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public Boolean getWinner() {
		return this.winner;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setWinner(Boolean winner) {
		this.winner = winner;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<ProducerModel> getProducers() {
		return producers;
	}

	public void setProducers(Set<ProducerModel> producers) {
		this.producers = producers;
	}

	public Set<StudioModel> getStudios() {
		return studios;
	}

	public void setStudios(Set<StudioModel> studios) {
		this.studios = studios;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieModel)) return false;
        return id != null && id.equals(((MovieModel) o).getId());
    }
 
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

	@Override
	public String toString() {
		return "MovieModel [id=" + id + ", year=" + year + ", title=" + title + ", winner=" + winner + ", producers="
				+ producers + "]";
	}
    
    
}
