package com.texoit.goldenrbaward.data.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.sun.istack.NotNull;
import com.texoit.goldenrbaward.domain.entities.IStudio;

@Entity(name = "Studio")
@Table(name = "tb_studio")
public class StudioModel implements IStudio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_studio")
	private Long id;

	@NotNull
	@Column(name = "name")
	private String name;

	@NaturalId
	@NotNull
	@Column(name = "slug")
	private String slug;

	@ManyToMany(mappedBy = "studios")
	private Set<MovieModel> movies = new HashSet<>();

	public StudioModel(String name, String slug) {
		super();
		this.name = name;
		this.slug = slug;
	}

	public StudioModel() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public String getSlug() {
		return this.slug;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Set<MovieModel> getMovies() {
		return movies;
	}

	public void setMovies(Set<MovieModel> movies) {
		this.movies = movies;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		StudioModel s = (StudioModel) o;
		return Objects.equals(slug, s.slug);
	}

	@Override
	public int hashCode() {
		return Objects.hash(slug);
	}

}
