package com.texoit.goldenrbaward.data.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.sun.istack.NotNull;
import com.texoit.goldenrbaward.domain.entities.IProducer;

@Entity(name = "Producer")
@Table(name = "tb_producer")
public class ProducerModel implements IProducer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_producer")
	private Long id;

	@NotNull
	@Column(name = "name")
	private String name;

	@NaturalId
	@NotNull
	@Column(name = "slug")
	private String slug;

	@ManyToMany(mappedBy = "producers")
	private Set<MovieModel> movies = new HashSet<>();

	public ProducerModel(String name, String slug) {
		super();
		this.name = name;
		this.slug = slug;
	}
	
	public ProducerModel() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getSlug() {
		return slug;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<MovieModel> getMovies() {
		return movies;
	}

	public void setMovies(Set<MovieModel> movies) {
		this.movies = movies;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ProducerModel p = (ProducerModel) o;
		return Objects.equals(slug, p.slug);
	}

	@Override
	public int hashCode() {
		return Objects.hash(slug);
	}

	@Override
	public String toString() {
		return "ProducerModel [id=" + id + ", name=" + name + ", slug=" + slug + "]";
	}
	
	
}
