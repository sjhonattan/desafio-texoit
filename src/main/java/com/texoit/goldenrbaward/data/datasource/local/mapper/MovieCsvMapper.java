package com.texoit.goldenrbaward.data.datasource.local.mapper;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import com.opencsv.bean.CsvBindByPosition;
import com.texoit.goldenrbaward.data.models.MovieModel;
import com.texoit.goldenrbaward.data.models.ProducerModel;
import com.texoit.goldenrbaward.data.models.StudioModel;

public class MovieCsvMapper {

	@CsvBindByPosition(position = 0)
	private String year;

	@CsvBindByPosition(position = 1)
	private String title;

	@CsvBindByPosition(position = 2)
	private String studios;

	@CsvBindByPosition(position = 3)
	private String producers;

	@CsvBindByPosition(position = 4)
	private String winner;

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	@Override
	public String toString() {
		return "MovieMapper [year=" + year + ", title=" + title + ", studios=" + studios + ", producers=" + producers
				+ ", winner=" + winner + "]";
	}

	public MovieModel parseH2() {
		MovieModel movie = new MovieModel();

		movie.setTitle(this.getTitle());
		movie.setYear(Integer.parseInt(this.getYear()));
		movie.setWinner(this.getWinner() != null && !this.getWinner().isEmpty());

		Set<StudioModel> studios = Arrays.asList(this.getStudios().split(",")).stream().map(e -> new StudioModel(e.trim(),e.toLowerCase().replaceAll("\\s+","").trim()))
				.collect(Collectors.toSet());

		Set<ProducerModel> producers = Arrays
				.asList(this.getProducers().replace("/ and /gi", ",").replaceAll("\\b, and\\b", ",").replaceAll("\\band\\b", ",").split(","))
				.stream().map(e -> new ProducerModel(e.trim(),e.toLowerCase().replaceAll("\\s+","").trim())).collect(Collectors.toSet());

		studios.stream().forEach(movie::addStudio);
		producers.stream().forEach(movie::addProducer);

		return movie;
	}

}
