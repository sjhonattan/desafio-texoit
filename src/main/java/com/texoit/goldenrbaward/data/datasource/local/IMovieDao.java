package com.texoit.goldenrbaward.data.datasource.local;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.texoit.goldenrbaward.data.models.MovieModel;
import com.texoit.goldenrbaward.domain.entities.IMovie;

public interface IMovieDao extends JpaRepository<MovieModel, Long>{
	
	@Query("SELECT m FROM Movie m WHERE m.winner is TRUE")
	List<MovieModel> getWinningMovies() throws Exception ;
}
