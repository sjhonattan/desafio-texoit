package com.texoit.goldenrbaward.data.datasource.local;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.texoit.goldenrbaward.data.models.ProducerModel;

public interface IProducerDao extends JpaRepository<ProducerModel, Long>{
	@Query("	SELECT " + 
			"		distinct p " + 
			"	FROM " + 
			"		Producer p " + 
			" 		JOIN p.movies m "	+
			"	WHERE " + 
			"		 m.winner is true ")
	List<ProducerModel> getAllWinningProducers() ;
	
	ProducerModel findBySlug(String slug) ;
}
