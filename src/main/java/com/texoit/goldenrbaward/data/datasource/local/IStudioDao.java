package com.texoit.goldenrbaward.data.datasource.local;

import org.springframework.data.jpa.repository.JpaRepository;

import com.texoit.goldenrbaward.data.models.StudioModel;

public interface IStudioDao extends JpaRepository<StudioModel, Long>{
	StudioModel findBySlug(String slug) ;
}
