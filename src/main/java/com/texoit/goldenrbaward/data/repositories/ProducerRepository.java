package com.texoit.goldenrbaward.data.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.texoit.goldenrbaward.data.datasource.local.IMovieDao;
import com.texoit.goldenrbaward.data.datasource.local.IProducerDao;
import com.texoit.goldenrbaward.data.models.MovieModel;
import com.texoit.goldenrbaward.data.models.ProducerModel;
import com.texoit.goldenrbaward.data.models.ProducerWinnerIntervalModel;
import com.texoit.goldenrbaward.domain.entities.IMovie;
import com.texoit.goldenrbaward.domain.entities.IProducer;
import com.texoit.goldenrbaward.domain.entities.IProducerWinnerInterval;
import com.texoit.goldenrbaward.domain.repositories.IProducerRepository;

public class ProducerRepository implements IProducerRepository {

	@Autowired
	private IMovieDao movieDao;

	@Autowired
	private IProducerDao producerDao;

	@Override
	public List<IProducerWinnerInterval> getProducersWinningIntervalWith2orMoreAwards(boolean minorInterval) throws Exception {
		try {
			List<MovieModel> winningMovies = movieDao.getWinningMovies();

			List<ProducerModel> allWinningProducers = producerDao.getAllWinningProducers();

			List<IProducerWinnerInterval> intervals = new ArrayList<IProducerWinnerInterval>(0);

			for (IProducer producer : allWinningProducers) {

				List<MovieModel> producerWinningMovies = winningMovies.stream()
						.filter(m -> m.getProducers().stream().anyMatch(p -> p.equals(producer)))
						.collect(Collectors.toList());

				if (producerWinningMovies.size() <= 1) {
					continue;
				}

				Collections.sort(producerWinningMovies, new Comparator<IMovie>() {
					@Override
					public int compare(IMovie m1, IMovie m2) {
						return m1.getYear().compareTo(m2.getYear());
					}
				});
				
				ProducerWinnerIntervalModel result = null;
				for (int i = 0; i < producerWinningMovies.size() - 1; i++) {

					Integer year = producerWinningMovies.get(i).getYear();
					Integer nextYear = producerWinningMovies.get(i + 1).getYear();
					Integer interval = Math.abs(year - nextYear);

					if (i == 0) {
						result = new ProducerWinnerIntervalModel(producer.getName(), interval, year, nextYear);
					} else if ((minorInterval && interval < result.getInterval()) || (!minorInterval && interval > result.getInterval())) {
						result = new ProducerWinnerIntervalModel(producer.getName(), interval, year, nextYear);
					}

				}

				intervals.add(result);
			}

			return intervals;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}


}
